.. #########################################################
.. version: $Id$
.. src: xen/include/public/vcpu.h@RELEASE-4.13.0
.. #########################################################

runstate
========

This document describes the *runstate* hypercall that can be used by a guest to
have runtime information on current state and running time of a vCPU.

Runstate information and usage
------------------------------

This is described in public/vcpu.h header.

The runstate contains the following information:

- the current runstate of the vCPU (state). This can have the following values:

  - RUNSTATE_running: vCPU is currently running.

  - RUNSTATE_runnable: vCPU is runnable but not currently scheduled.

  - RUNSTATE_blocked: vCPU is blocked (aka idle) and therefore not runnable.

  - RUNSTATE_offline: vCPU is currently offline.

- when the current state was entered (state_entry_time). Upper bit of this
  field is used to inform that the information is currently being updated.

- the time spent in each possible runstate (time). The sum of each time is
  guaranted not to drift from the system time.

Hypercall interface
-------------------

This is described in public/vcpu.h header.

To different interfaces are available to get the runstate information:

- VCPUOP_get_runstate_info: this call can be used to retrieve the runstate
  information on demand.

- VCPUOP_register_runstate_memory_area: This call can be used to register
  a memory area where the runstate information should be updated by Xen during
  runtime.

Runtime interface
-----------------

Once an area has been register by the guest through the
VCPUOP_register_runstate_memory_area hypercall, Xen will update the content
during context switches with current information.

The following restrictions must be taken into account:

- the content will only be updated if the area was accessible in the current
  state of the guest during the context switch. As a consequence, if the
  virtual address is not mapped (this can happen if a Linux has KPTI activated
  and is currently running in user mode while the Xen context switch happen)
  the data will not be updated.

- Xen is setting the bit XEN_RUNSTATE_UPDATE in state_entry_time atomically
  when the area is updated. As a consequence a guest should not access the
  runstate data while this bit is set.

- Online one area may be registered for a vCPU, if several areas are registered
  only the last registered one will be updated.

- If the virtual address registered is not mapped, no error will be generated.

