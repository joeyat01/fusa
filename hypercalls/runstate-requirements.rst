===============
Runstate Requirements 
===============
---------------
1: High-Level Requirements 
---------------

1.1.0: Functional Requirements 
    1.1.1: The Xen Hypervisor shall provide guests a hypercall interface to allow them to retrieve runstate information on their vCPU at real-time. 
    
    1.1.2: Xen hypervisor shall store the runstate information in the memory area allocated [#note]_ by the guest
    
    .. [#note] Need to define what we mean by memory allocation e.g. malloc? Dynamic or static?
    
    1.1.3: Xen Hypervisor shall retrieve and provide the runstate for the guest upon the request 
    
    1.1.4: The Xen Hypervisor shall only update the last registered memory area for the vCPU’s runstate 

1.2.0: Interface Requirements 
    1.2.1: The Xen Hypercall interface shall allow arguments for guests to allocate a memory area in which the runstate information will be stored for the selected guest
    
    1.2.2: The Xen Hypercall interface shall allow guests to retrieve the runstate.

1.3.0: Robustness Requirements 
    1.3.2: While the Xen hypervisor is updating the runstate memory area the access for guests shall be blocked
    
     Note: Xen Hypervisor will only update the content of the runstate memory area if the memory area is accessible at the time of the context switch

---------------
2: Low-Level Requirements 
---------------
2.0.1: The Xen Hypervisor vCPU shall only have four states: 

    -	RUNSTATE_running: vCPU is currently running.
    -	RUNSTATE_runnable: vCPU is runnable but not currently scheduled.
    -	RUNSTATE_blocked: vCPU is blocked (aka idle) and therefore not runnable.
    -	RUNSTATE_offline: vCPU is currently offline.
    
2.0.2: The Xen Hypervisor runstate shall include information about the current state of the vCPU, current time the state was entered, and the time spent in each possible state.  
